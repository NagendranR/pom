package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class FindLeadsPage extends ProjectMethods {
	
	public FindLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH,using="//input[@name='id']/following::input[1]") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="//button[contains(text(),'Find Leads')]") WebElement eleFindLeadsButton;
	@FindBy(how=How.XPATH,using="(//table[@class='x-grid3-row-table']/tbody/tr[1]/td[1]//a)[1]") WebElement eleFirstRow;
	public FindLeadsPage enterFirstName(String data)

	{
		clearAndType(eleFirstName, data);
		return this;
	}
	
	public FindLeadsPage clickFindLeadsButton()
	{
		click(eleFindLeadsButton);
		return this;
	}
	
	public ViewLeadsPage clickFirstResultingLead()
	{
		click(eleFirstRow);
		return new ViewLeadsPage();
	}
}
