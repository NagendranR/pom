package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class LeadsPage extends ProjectMethods {
	public LeadsPage() {
		// apply PageFactory
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.LINK_TEXT, using = "Create Lead")
	WebElement eleCreateLead;
	@FindBy(how = How.LINK_TEXT, using = "Find Leads")
	WebElement eleFindLeads;

	public CreateLeadsPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadsPage();
	}

	public FindLeadsPage clickFindLeads() {

		click(eleFindLeads);
		return new FindLeadsPage();
	}
}
