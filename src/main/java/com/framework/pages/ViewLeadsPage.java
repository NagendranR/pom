package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods {
	
	public ViewLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.ID,using="viewLead_firstName_sp") WebElement eleFirstName;
	@FindBy(how=How.XPATH,using="//a[text()='Edit']") WebElement eleEditButton;
	@FindBy(how=How.ID,using="viewLead_companyName_sp") 
	WebElement eleCompanyName;
	public ViewLeadsPage verifyFirstName(String data)
	{
		verifyExactText(eleFirstName, data);
		return this;
	}
	
	
	public EditLeadPage clickEditButton()
	{
		click(eleEditButton);
		return new EditLeadPage();
	}
	
	public ViewLeadsPage verifyUpdatedCompanyName(String data)
	{
		verifyPartialText(eleCompanyName, data);
		return this;
	}
}
