package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadsPage extends ProjectMethods {
	
	public CreateLeadsPage()
	{
		PageFactory.initElements(driver, this); 
	}

	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.NAME,using="submitButton") WebElement eleSubmitButton;
	
	public CreateLeadsPage enterCompanyName(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleCompanyName, data);
		return this; 
	}
		
		public CreateLeadsPage enterFirstName(String data) {
			//WebElement eleUsername = locateElement("id", "username");
			clearAndType(eleFirstName, data);
			return this; 
		}
			
			public CreateLeadsPage enterLastName(String data) {
				//WebElement eleUsername = locateElement("id", "username");
				clearAndType(eleLastName, data);
				return this; 
			}
			
			public ViewLeadsPage clickCreateLeadbutton() {
				//WebElement eleLogin = locateElement("class", "decorativeSubmit");
			   click(eleSubmitButton);
			    /*HomePage hp = new HomePage();
			    return hp;*/ 
			    return new ViewLeadsPage();
			}
		
		
		

}
