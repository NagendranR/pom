package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	
 public EditLeadPage() 
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how=How.XPATH,using="//input[@id='updateLeadForm_companyName']") WebElement eleCompanyName;
	@FindBy(how=How.XPATH,using="//input[@name='submitButton' and @value='Update']") WebElement eleUpdateButton;
	public EditLeadPage editCompanyName(String data)
	{
		clearAndType(eleCompanyName, data);
	return this;
	}
	
	public ViewLeadsPage clickUpdateButton()
	{
		click(eleUpdateButton);
		return new ViewLeadsPage();
	}
}
