package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class CreateLead extends ProjectMethods {
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLead";
		testDescription = "Creating new Lead";
		testNodes = "Leads";
		author = "Nagu";
		category = "Smoke";
		dataSheetName = "TC001";
	}

	@Test(dataProvider = "fetchData")
	public void createLead(String username, String password, String companyName, String firstName, String lastName) {
		// LoginPage lp=new LoginPage();

		new LoginPage().enterUsername(username).enterPassword(password).clickLogin().clickCrmsfa().clickLeads()
				.clickCreateLead().enterCompanyName(companyName).enterFirstName(firstName).enterLastName(lastName)
				.clickCreateLeadbutton().verifyFirstName(firstName);

	}
}
