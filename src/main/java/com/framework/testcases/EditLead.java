package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class EditLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_EditLead";
		testDescription = "Editing existing Lead";
		testNodes = "Leads";
		author = "Nagu";
		category = "smoke";
		dataSheetName = "TC002";
	}
	
	@Test(dataProvider = "fetchData")
	public void editLead(String username,String password,String firstName,String companyName,String updatedCompanyName)
	{
		new LoginPage().
		enterUsername(username).
		enterPassword(password).
		clickLogin().
		clickCrmsfa()
		.clickLeads().
		clickFindLeads().
		enterFirstName(firstName).
		clickFindLeadsButton().
		clickFirstResultingLead()
		.clickEditButton().
		editCompanyName(companyName).
		clickUpdateButton().
		verifyUpdatedCompanyName(updatedCompanyName);
	}

}


